package byeworld

import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.fail

internal class ServiceTest {

    @Test
    fun waste_time() {
        runBlocking {
            println("Changing test also")
            delay(60000)
        }
    }
}