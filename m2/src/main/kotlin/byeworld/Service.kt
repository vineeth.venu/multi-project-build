package byeworld

import core.loggerFor

internal object Service {
    fun sayByeWorld(): String {
        return "Hello World!".also { loggerFor<Service>().info(it) }
    }
}

