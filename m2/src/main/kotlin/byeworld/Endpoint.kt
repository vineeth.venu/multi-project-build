package byeworld

import byeworld.Service.sayByeWorld
import core.ApiEndpoint
import io.vertx.core.json.Json
import io.vertx.ext.web.Router
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class Endpoint: ApiEndpoint {
    override fun addTo(router: Router) {
        router.get("/hello-world-2").handler { context ->
            CoroutineScope(Dispatchers.Default).launch {
                context.response()
                    .setStatusCode(200)
                    .end(Json.encode(mapOf(
                        "message" to sayByeWorld(),
                        "source" to "API"
                    )))
            }
        }
    }
}