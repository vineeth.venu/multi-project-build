plugins {
  kotlin("jvm")
  java
}

dependencies {
  compile(project(":common"))
  testCompile("org.junit.jupiter:junit-jupiter-api:5.3.1")
  testCompile("org.junit.jupiter:junit-jupiter-engine:5.3.1")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions {
    jvmTarget = "1.8"
    freeCompilerArgs = listOf("-Xjsr305=strict")
  }
}

tasks {
  "test"(Test::class) {
    useJUnitPlatform()
  }
}