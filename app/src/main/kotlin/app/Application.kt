package app

import core.ApiVerticle
import io.vertx.core.Vertx.vertx
import helloworld.Endpoint as HelloWorldEndpoint
import byeworld.Endpoint as ByeWorldEndpoint
object Application    {

    @JvmStatic
    fun main(args : Array<String>) {
        val vertx = vertx()
        println("App")
        vertx.deployVerticle(
            ApiVerticle(
                port = 8080,
                endpoints = listOf(
                    HelloWorldEndpoint(),
                    ByeWorldEndpoint()
                )
            )
        )
    }

}

