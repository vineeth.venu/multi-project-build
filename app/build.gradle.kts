import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

val mainClass = "app.Application"

plugins {
  kotlin("jvm")
  application
  id("com.github.johnrengelman.shadow") version "4.0.4"
}

application {
  mainClassName = mainClass
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions {
    jvmTarget = "1.8"
    freeCompilerArgs = listOf("-Xjsr305=strict")
  }
}

tasks {
  named<ShadowJar>("shadowJar") {
    archiveBaseName.set("app")
    mergeServiceFiles()
    manifest {
      attributes(mapOf("Main-Class" to mainClass))
    }
  }
}

dependencies {
  compile(project(":m1"))
  compile(project(":m2"))
  compile(project(":common"))
  compile(kotlin("stdlib"))
}
