# Project with gradle submodules


## Project setup

There are two submodules in this project, `m1` and `m2`.

The other two submodules  are for common code `common`, and for bootstrapping everything `app`. 

`m1` and `m2` can be considered as submodules holding all the business logic of the application.
## Experiments

### 1. Can we speed up the pipeline with cached submodule builds? 

**Details:** 
When a submodule is changed, only that submodule should be rebuilt and tests for it run. 

We do not need to rebuild other submodules, or run their tests. Ofcourse, this implies that the submodules must be split such that they do not depend on each other.

This will save us a considerable amount of time in larger projects, as more often than not, work in a team is concentrated only in certain sections/or features of an application. If these modules were to be organized along the lines of those features, we might be able to save on pipeline build-time.


#### Result  
Yes, subsequent builds use the cache and prevent rebuilds, saving time.


#### Proof 

First, we made a commit with changes to all the submodules - a build for this was kicked off by Gitlab-CI 

**Commit:** https://gitlab.com/vineeth.venu/multi-project-build/-/commit/4b0048fb8cbd3dc077c36e716a62ada8db4baaa3

**Job:** https://gitlab.com/vineeth.venu/multi-project-build/-/jobs/499402468

From the output, we see all the individual modules were built. 

Please note: 

```
> Task :common:build
> Task :app:build
> Task :m1:build
> Task :m2:build
```

Second, we then created a commit **with changes only to the M2 submodule**. 

**Commit:** https://gitlab.com/vineeth.venu/multi-project-build/-/commit/149b0def3e993634cbb2a8db80c605c15d782b62

**Job:** https://gitlab.com/vineeth.venu/multi-project-build/-/jobs/499404803 

Let's look at the output again 

```
> Task :common:build UP-TO-DATE
> Task :app:build
> Task :m1:build UP-TO-DATE
> Task :m2:build
```

**Do note that only the builds for app:build (this this bootstraps the application) and m2:build were run.** (NOTE the UP-TO-DATE state) 

The output for the `m1` and `common` submodules were picked up from the cache

Hence we can confidently say that this approach will provide us faster builds, by avoiding rebuilding infrequently used modules during consecutive pipeline runs.


### Further Reading 

1. Why is this important? https://www.gocd.org/2018/10/30/measure-continuous-delivery-process/ 



