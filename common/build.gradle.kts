
val vertxVersion = "3.8.3"

plugins {
  kotlin("jvm")
  java
}

dependencies {
  compile(kotlin("stdlib"))
  compile("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
  compile("io.vertx:vertx-core:$vertxVersion")
  compile("io.vertx:vertx-lang-kotlin-coroutines:$vertxVersion")
  compile("io.vertx:vertx-lang-kotlin:$vertxVersion")
  compile("io.vertx:vertx-web:$vertxVersion")
  compile("io.vertx:vertx-web-client:$vertxVersion")

  compile("ch.qos.logback:logback-classic:1.2.3")
  compile("org.jetbrains.kotlinx:kotlinx-coroutines-slf4j:1.3.3")
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
  kotlinOptions {
    jvmTarget = "1.8"
    freeCompilerArgs = listOf("-Xjsr305=strict")
  }
}