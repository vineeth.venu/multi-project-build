package core

import io.vertx.ext.web.Router

interface ApiEndpoint {
    fun addTo(router: Router)
}

