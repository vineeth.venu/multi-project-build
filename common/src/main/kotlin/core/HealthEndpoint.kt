package core

import io.vertx.core.json.Json
import io.vertx.ext.web.Router

class HealthEndpoint : ApiEndpoint {
    override fun addTo(router: Router) {
        router.get("/live").handler {
            it.response()
                .setStatusCode(200)
                .end(Json.encode(
                    mapOf("status" to "OK")
                ))
        }
    }
}

