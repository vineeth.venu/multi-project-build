package core

import io.vertx.ext.web.Router.router
import io.vertx.kotlin.core.http.listenAwait
import io.vertx.kotlin.coroutines.CoroutineVerticle

class ApiVerticle(
    private val port: Int,
    private val endpoints: List<ApiEndpoint>
) : CoroutineVerticle() {

    override suspend fun start() {
        vertx.createHttpServer()
            .requestHandler(routerWithEndpoints())
            .listenAwait(port)
            .also { log("Ready") }
    }

    private fun routerWithEndpoints() =
        router(vertx).also { router ->
            endpoints.plus(HealthEndpoint()).forEach {
                it.addTo(router)
                log("Configured -> ${it.javaClass.simpleName}")
            }
        }

    private fun log(message: String) {
        loggerFor<ApiVerticle>().info(message)
    }
}