package helloworld

import core.ApiEndpoint
import helloworld.Service.sayHelloWorld
import io.vertx.core.json.Json
import io.vertx.ext.web.Router

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class Endpoint : ApiEndpoint {
    override fun addTo(router: Router) {
        router.get("/hello-world").handler { context ->
            CoroutineScope(Dispatchers.Default).launch {
                context.response()
                    .setStatusCode(200)
                    .end(Json.encode(mapOf(
                        "message" to sayHelloWorld(),
                        "source" to "API"
                    )))
            }
        }
    }
}