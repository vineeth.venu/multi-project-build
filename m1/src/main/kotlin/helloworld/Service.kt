package helloworld

import core.loggerFor

object Service {
    internal fun sayHelloWorld() : String {
        return "Hello World!".also { loggerFor<Service>().info(it) }
    }
}
