
plugins {
  kotlin("jvm") version "1.3.71" apply false
  java
}

allprojects {
  group = "org.testing.modularity"
  version = "1.0.0"
  repositories {
    jcenter()
  }
}

subprojects {
  repositories {
    jcenter()
  }
}
